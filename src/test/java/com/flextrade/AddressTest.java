package com.flextrade;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;



public class AddressTest {

    Address user1 = new Address("EC14 5DJ", "26");

    @Test
    public void test_a_new_address_can_be_created_and_returns_correct_values(){
        assertEquals("Postcode was not as expected", "EC14 5DJ",user1.getPostcode());
        assertEquals("House number was not as expected", "26", user1.getHouseNumber());

        //using Hamcrest
        assertThat("Postcode was not as expected", user1.getPostcode(), equalTo("EC14 5DJ"));
        assertThat("House Number was not as expected", user1.getHouseNumber(), equalTo("26"));
    }

    @Test
    public void test_two_address_objects_are_equal(){
        Address user2 = new Address("EC14 5DJ", "26");
        assertEquals("Equal and hashcode for Address class are not overriden successfully", user2, user1);

        //using Hamcrest
        assertThat("Equal and hashcode for Address class are not overriden successfully", user2, equalTo(user1));
    }

    @Test
    public void test_can_set_address_postcode(){
        user1.setPostcode("BR6 9PN");
        assertThat("The postcode was not set correctly","BR6 9PN", equalTo(user1.getPostcode()));
    }

    @Test
    public void test_can_set_address_housenumber(){
        user1.setHouseNumber("1078");
        assertThat("The house number was not set correctly","1078", equalTo(user1.getHouseNumber()));
    }
}
