package com.flextrade;

import com.flextrade.exceptions.*;

public class App {

    public static void main(String[] args) {
        Exchange exchange = new Exchange();
        Address address1 = new Address("BR6123", "12");
        Address address2 = new Address("TA56789", "105");
        Address address3 = new Address("EC145DJ", "34a");
        Address address4 = new Address("EC46111", "200");
        Address address5 = new Address("WAAAA", "2235"); // used to test AddressNotFoundException
        Address address6 = new Address("CCCC", "567");

        try {
            exchange.addConnection(address1);
            exchange.addConnection(address2);
            exchange.addConnection(address3);
            exchange.addConnection(address4);
            exchange.addConnection(address6);

            System.out.println("Number of Addresses currently connected: " + exchange.getAddressCount());


            String telephoneNum1 = exchange.getNumberForAddress(address1);
            System.out.println("Number associated with address 1: " + telephoneNum1);
            String telephoneNum2 = exchange.getNumberForAddress(address2);
            System.out.println("Number associated with address 2: " + telephoneNum2);
            String telephoneNum3 = exchange.getNumberForAddress(address3);
            System.out.println("Number associated with address 3: " + telephoneNum3);
            String telephoneNum4 = exchange.getNumberForAddress(address4);
            System.out.println("Number associated with address 4: " + telephoneNum4);

            exchange.deleteConnection(address4);  //testing removal of connection
            //exchange.deleteConnection(address5);  //used to test AddressNotFoundException
            exchange.showConnections();

            exchange.connectCall("0207123567", telephoneNum1);
            exchange.connectCall("0207444555", telephoneNum2);
            exchange.connectCall("0207222222", telephoneNum3);
            exchange.showDetailsOfNumbersConnected();
            //exchange.disconnectCall(30);  // to see this work you need to set callId to 30 in exchange.connectCall
            //exchange.showDetailsOfNumbersConnected(); // check the list afterwards, to ensure it is disconnected
            exchange.maintainLine(address1);
            exchange.getAddressForNumber(telephoneNum1);
            System.out.println("The maximum number of supported users of the exchange is: " + exchange.getMaxCountUsers());
            System.out.println("Checking to see if the number is busy for address6 : " + exchange.isLineBusy(address6));
        } catch (ExchangeFullException e) {
            e.printStackTrace();
        } catch (AddressAlreadyInUseException e) {
            e.printStackTrace();
        } catch (NumberDoesNotExistException e) {
            e.printStackTrace();
        } catch (NumberBusyException e) {
            e.printStackTrace();
        } catch (AddressNotFoundException e) {
            e.printStackTrace();
        } catch (NumberNotFoundException e) {
            e.printStackTrace();
        }
    }
}


