package com.flextrade.exceptions;

public class ExchangeFullException extends Exception {

    public ExchangeFullException() {}

    public ExchangeFullException(String message){
        super(message);
    }
}
