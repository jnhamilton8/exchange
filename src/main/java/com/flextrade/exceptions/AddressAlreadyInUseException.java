package com.flextrade.exceptions;

public class AddressAlreadyInUseException extends Exception {

    public AddressAlreadyInUseException(){}

    public AddressAlreadyInUseException(String message) {
        super(message);
    }
}
