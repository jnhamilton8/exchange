package com.flextrade.exceptions;

public class NumberBusyException extends Exception {

    public NumberBusyException (String message){
        super(message);
    }
}
