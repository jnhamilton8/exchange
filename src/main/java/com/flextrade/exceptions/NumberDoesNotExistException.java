package com.flextrade.exceptions;

public class NumberDoesNotExistException extends Exception {

    public NumberDoesNotExistException(String message){
        super(message);
    }
}
