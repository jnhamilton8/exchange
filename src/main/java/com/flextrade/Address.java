package com.flextrade;

public class Address {

	private String postcode;
	private String houseNumber;

	public Address(String postcode, String houseNumber) {
		this.postcode = postcode;
		this.houseNumber = houseNumber;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	@Override
	public String toString() {
		return "Address{" +
				"postcode='" + postcode + '\'' +
				", houseNumber='" + houseNumber + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Address address = (Address) o;

		if (!postcode.equals(address.postcode)) return false;
		return houseNumber.equals(address.houseNumber);
	}

	@Override
	public int hashCode() {
		int result = postcode.hashCode();
		result = 31 * result + houseNumber.hashCode();
		return result;
	}
}
