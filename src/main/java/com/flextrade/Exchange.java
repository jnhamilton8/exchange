package com.flextrade;

import com.flextrade.exceptions.*;
import com.sun.nio.sctp.PeerAddressChangeNotification;

import java.util.*;

/**
 * Manages all aspects of the Exchange including adding and removing connections,
 * connecting calls and call maintenance.
 *
 * @author jane hamilton
 */

public class Exchange {

    private HashMap<String, Address> connections = new HashMap<String, Address>();
    private HashMap<String, Integer> numbersConnected = new HashMap<String, Integer>();
    private static final int MAXXUSERS = 5;
    private int maxNumUsers = 0;
    private int callId;

    public Exchange(){}

    /**
     * Generates a telephone number, associates telephone number with address and stores in the connections map
     * @param  address   the address to be connected
     * @return The telephone number associated with the address
     * @exception ExchangeFullException if the maximum number of users is exceeded.
     * @exception AddressAlreadyInUseException if the address has already been added as a connection.
     */

    public String addConnection(Address address) throws ExchangeFullException, AddressAlreadyInUseException {

        String telephoneNumber = createTelephoneNumber();

        if (connections.containsKey(telephoneNumber)) {
            throw new AddressAlreadyInUseException("Only one telephone number allowed per address");
        }

        if(maxNumUsers <= MAXXUSERS){
            connections.put(telephoneNumber, address);
            maxNumUsers++;
        }else{
            throw new ExchangeFullException("The exchange is full, no further users can be added");
        }
        return telephoneNumber;
    }

     /**
     * Displays the addresses and telephone numbers currently in the map
     */

    public void showConnections(){
        detailsOfConnections(connections);
    }

    private void detailsOfConnections(Map connections){
        Iterator<String> keysetIterator = connections.keySet().iterator(); //set up iterator

        System.out.println("Connection Details are:");
        while(keysetIterator.hasNext()){ //iterate over keys and print details
            String key = keysetIterator.next();
            System.out.println("Telephone number: " + key + " Associated with: " + connections.get(key));
        }
        System.out.println("-------------------------------");
    }

    /**
     * Removes the address and telephone number from the connections map
     * @param  address   the address to be removed
     * @exception AddressNotFoundException if the address is not in the connections map
     */

    public void deleteConnection(Address address) throws AddressNotFoundException {

        if (address == null || !connections.containsValue(address)) {
            throw new AddressNotFoundException("That address is not found: " + address);
        }

        Iterator<Map.Entry<String, Address>> iter = connections.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, Address> entry = iter.next();
            if (address.equals(entry.getValue())) {
                System.out.println("Removing telephone number " + entry.getKey() + " from " + entry.getValue());
                iter.remove();
            }
        }
    }

    /**
     * Connects a source and destination number and simulates connecting the two by generating a unique callId
     * and inserting this alongside each number in the numbersConnected map
     * @param  sourceNumber   the caller's telephone number
     * @param  destinationNumber  the number the caller wishes to dial
     * @exception NumberBusyException if the source and destination number are already connected, simulated by
     * checking if they are already present in the numbersConnected map
     * @exception NumberDoesNotExistException if the number the caller is dialling does not exist in the connections map
     */

    public void connectCall(String sourceNumber, String destinationNumber) throws NumberBusyException, NumberDoesNotExistException {

        //if the number you are trying to call exists
        if (connections.containsKey(destinationNumber)) {
            //if the source or destination number are not already connected
            if (!numbersConnected.containsKey(sourceNumber) || !numbersConnected.containsKey(destinationNumber)) {

                //create random unique callId
                //callId = 30; //used for testing only for static call id. To be removed when tests are developed!
                callId = createRandomCallId();

                //insert CallId and corresponding sourceNumber and destinationNumber into Hashmap
                numbersConnected.put(sourceNumber, callId);
                numbersConnected.put(destinationNumber, callId);

                //connect the call
                System.out.println("Call connected between " + sourceNumber + " and " + destinationNumber);
            }else {
                throw new NumberBusyException("The number is currently busy.  Please try again later");
            }
        } else {
            throw new NumberDoesNotExistException("The number you are dialling does not exist. Please check the number and redial " + "Source: " + sourceNumber + "Destination " + destinationNumber);
        }
    }

    private int createRandomCallId() {

        Random rand = new Random();
        callId = rand.nextInt(30);

        if (numbersConnected.containsValue(callId) || callId == 0) {
            callId = rand.nextInt(30);
            createRandomCallId();
        }
        return callId;
    }

    /**
     * Displays details of the telephone numbers currently connected
     */

    public void showDetailsOfNumbersConnected(){
        detailsOfNumbersConnected(numbersConnected);
    }

    private void detailsOfNumbersConnected(Map numbersConnected){
        Iterator<String> keysetIterator = numbersConnected.keySet().iterator(); //set up iterator

        System.out.println("Source and Destination numbers will have the same callId as they are connected.  Details of numbers currently connected are: ");
        while(keysetIterator.hasNext()){ //iterate over keys and print details
            String key = keysetIterator.next();
            System.out.println("Number " + key + " CallId: " + numbersConnected.get(key));
        }
        System.out.println("-------------------------------");
    }

    /**
     * Simulates disconnecting the call by removing the source and destination number associated with the callId
     * @param  callId the ID number associated with the source and destination numbers which are currently connected
     */

    public void disconnectCall(int callId){

        for (String num : numbersConnected.keySet()) {
            if (numbersConnected.get(num).equals(callId)) {
                System.out.println("Disconnecting call for " + num);
            }
        }

        /*remove(callId) would only remove the first instance, we have an instance for sourceNumber and
        destinationNumber and need to remove both */
        numbersConnected.values().removeAll(Collections.singleton(callId));
    }

    public void maintainLine(Address address)throws AddressNotFoundException{

        if (address == null || !connections.containsValue(address)) {
            throw new AddressNotFoundException("That address is not found so line maintenance cannot continue: " + address);
        }
        System.out.println("Performing maintenance on line for address: " + address);
    }

    public void getAddressForNumber(String number) throws NumberNotFoundException{

        if(connections.containsKey(number)){
            Address address = connections.get(number);
            System.out.println("The address associated with telephone number " + number + " is: " + address);
        } else {
            throw new NumberNotFoundException("The telephone number does not exist: " + number);
        }
    }

    public int getAddressCount() {
        int numAddressesRegistered = connections.size();
        return numAddressesRegistered;
    }

    public String getNumberForAddress(Address address) throws AddressNotFoundException {

        String numForAddress = "";

        if (address == null || !connections.containsValue(address)) {
            throw new AddressNotFoundException("That address is not found so the number cannot be returned: " + address);
        }

        for (String tele : connections.keySet()) {  //returns "set" view of keys in the map
            if (connections.get(tele).equals(address)) {  //if the value mapped to the key = the address passed in
                numForAddress = tele;
            }
        }
        return numForAddress;
    }

    public boolean isLineBusy(Address address) throws AddressNotFoundException{
        //get the telephone number for the address out of connections map
        String numForAddress = getNumberForAddress(address);

        //if the telephone number is in the numbersConnect map then it is busy
        if(numbersConnected.containsKey(numForAddress)){
            return true;
        }
        return false; //otherwise it is free
    }

    public int getMaxCountUsers(){
        return MAXXUSERS;
    }


    private String createTelephoneNumber() {
        int min = 100000;
        int max = 999999;
        String suffix = "0207";
        String telephoneNum;

        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        telephoneNum = suffix + Integer.toString(randomNum);
        return telephoneNum;
    }
}
